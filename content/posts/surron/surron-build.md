---
title: My Surron - The ultimate mobility machine.
date: 2022-06-05
author: Arran

description: How I modded my street legal, electric dirt bike.
categories: [Gear, Travel]
tags: [Surron, DIY]


draft: false
---

[SeatRiser]: https://www.gruber-parts.com/shop/Sitzbankerh%C3%B6hung-50-mm-mit-Anbaukit-p207527817
[Alarm]: https://www.abus.com/eng/Home-Security/Personal-security/Alarmbox-RC
[Lock]: https://www.kryptonitelock.com/en/products/product-information/current-key/000808.html
[Multitool]: https://www.sks-germany.com/en/products/tom-18/
[Sprocket]: https://www.gruber-parts.com/shop/Kettenrad-Performance-Aluminium-T56-schwarz-p309237051
[ChainExtension]: https://www.gruber-parts.com/shop/Kettenverl%C3%A4ngerung-O-Ring-T56-p296586292
[HandleBars]: https://www.ergotec.de/en/products/lenker/sub/mountain-bike-lenker/produkt/riser-bar-70-31-8.html
[Headlight]: https://www.osram.de/ecat/Round%20VX80-WD-LED%20Zusatzscheinwerfer-LKW-Beleuchtung-Fahrzeugbeleuchtung/de/de/GPS01_3438884/ZMP_4061649/
[Mirror]: https://www.louis.de/artikel/gazzini-lenkerendenspiegel-udine-schwarz-links-oder-rechts-stueck/10039623
[Indicators]: https://www.conrad.com/p/devil-eyes-611000-led-turn-signal-quads-atvs-aluminium-1307607
[Bracket]: https://www.gruber-parts.com/shop/Kennzeichenhalter-SPORT-p218010356
[Pegs]: https://www.ycf-riding.com.au/cnc-ycf-aluminium-foot-peg-bk


{{< figure src="../images/b-portrait.jpg" caption="Deep in the woods">}}

The Surron is a complete hack for urban and rural mobility - Fly through the woods and nip between streets - Here's how I made my street-legal and off-road capable Surron.

---

## Part List ⚙️

Mounted:
- Surron L1E
- Seat riser, [SeatRiser]
- Handlebars, [Ergotech 70mm][HandleBars]
- Single rear mirror, [Gazzini][Mirror]
- Indicators, [Devil Eyes][Indicators]
- Numberplate braket, [GruberParts][Bracket]
- Front Headlight, [OSRAM VX80-WD][Headlight]
- LED Switches, Wires & Misc
- Tyres, MITAS-Terra-Force-MX-SM-70-100-19
- 56T [Sprocket] & [Chain Extension][ChainExtension]
- Foot-pegs, [YCF Pegs][Pegs]

Extras:
- Lock, [Kryptonite 1090][Lock]
- Vibration Alarm, [ABUS Alertbox RC][Alarm]
- Multi-tool, [TOM-18][Multitool]

---

## Size Adjustments 📏


A big priority was a comfortable ride and the stock Surron is quite a small bike - The very first thing I did was increase the height of the seat by 50mm with a [Seat Riser][SeatRiser].

The next thing I noticed was how forward my standing position was, so for a more upright ride, I swapped out the stock bars for a [Ergotech 70mm Riser Bar][HandleBars].
<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/seat/unmounted.jpg" caption="Unmounted seat with riser and side panels" class="limit-height-1">}}
{{< figure src="../images/seat/mounted.jpg" caption="With 50mm seat riser" class="limit-height-1">}}
</div>

---

## Street Mods / Electrics 🔌

From the factory, the L1E (Street Edition) is not a particularly pretty bike, I mean.. [look at it](../images/stock-l1e.jpg).

With a flimsy rear mud-guard hosting the rear tail light and indicators, plus an ugly-as-fuck headlight & blinker bundle - **Things had to change**.

The only limiting factor here, was keeping this road worthy - I made sure every component I used is "E-Approved" and legal for Mopeds in the United Nations Economic Commission for Europe (ECE). [What is E-Approved?](https://www.apauto.com.au/what-is-e-approved/)

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure style="" src="../images/wiring-l1e.png" caption="L1E Wiring Diagram" class="limit-height-1">}}
{{< figure style="" src="../images/stock-l1e.jpg" caption="Stock L1E 🤮" class="limit-height-1">}}
</div>


### Headlight / Front Indicators

I searched far and wide for a suitable headlight: road legal, bright and mountable! I was stoked to get a reply from Osram confirming the legality and including dimensions of the [Osram VX80-WD][Headlight].

Amazingly, This headlight fits perfectly inside the sport [headlight bracket](https://mall.sur-ron.com/products/sur-ron-lightbee-head-light-assy-bracket).

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/headlight/osram-vx80wd.png" caption="Osram VX80-WD Dimensions" class="limit-height-1">}}
{{< figure src="https://dammedia.osram.info/media/resource/900/osram-dam-15326400/Round%20VX80-WD%20LEDDL119-WD.jpg" class="limit-height-1">}}
</div>

Paired with a waterproof LED switch, the solution is really functional and __BRIGHT__.

Installation was straight-forward - I removed stock headlight and indicator housing, cut the wires to length and mounted the VX80 with the sport bracket onto the forks.
Next, I drilled a 20mm hole in the battery cover and placed the switch in, connected the three terminals to the headlight, power and ground respectively.

For the front [indicators][Indicators], I used two 3D printed brackets threaded with M6 holes - I mounted these onto the side of the forks and twisted them in.

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/headlight/switch.jpg" caption="Switch underside" class="limit-height-1">}}
{{< figure src="../images/headlight/on.jpg" caption="Indicator + Light on" class="limit-height-1">}}
</div>


### Tail light / Rear Indicators

I got a [steel bracket][Bracket] to mount underneath the seat, reused the stock Tail light and purchased another two [indicators][Indicators].

Wiring them up was easy enough, I also used a 5 pin waterproof plug to make it detachable.

```yml
# Rear Wiring Guide
Green = Ground
Cyan = Right Indicator
Orange = Left Indicator
Black = Tail Light, License Lamp
Green/Yellow = Brake Light
```

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/backlight/wires.jpg" caption="Rear wiring" class="limit-height-1">}}
{{< figure src="../images/backlight/rear-1.jpg" caption="WIP" class="limit-height-1">}}
{{< figure src="../images/backlight/mounted-close.jpg" caption="Mounted on the bike" class="limit-height-1">}}
</div>

### Rear view mirror

Not crazy interesting, but a mirror helps when driving on the street - I wanted a mirror that did not increase the width of the bike and was easy to move aside when riding off-road.

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/mirror/in.jpg" caption="Off-Road" class="limit-height-1">}}
{{< figure src="../images/mirror/out.jpg" caption="Street" class="limit-height-1">}}
</div>

---

## Performance 🚀

For tyres, I'm currently running two `MITAS-Terra-Force-MX-SM-70-100-19` - The traction on muddy trails is incredible and they're light enough as to not impede torque.

I've also swapped the stock 48T sprocket for a 56T - The extra torque is a major help on hill-climbs and the reduction of top speed is minimal. The 56T sprocket is perfect for my needs both on and off-road.

One really important upgrade I wish I had done sooner, was replacing the thin stock foot-pegs - I swapped them out for [YCF FootPegs][Pegs] which fit the Surron without modification.

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/performance/tyre.jpg" caption="Front Tyre" class="limit-height-1">}}
</div>

---

## Security 🔒

The Surron comes with a built-in GPS tracker called a [WeTrack2](https://www.iconcox.com/products/general-gps-vehicle-tracker-wetrack2.html) - The tracker supposedly has a "Movement Alarm" which will SMS you when the bike is moved, but I did not get the feature to work - However, I do receive an SMS when the battery is disconnected and I can always SMS the tracker to get it's current location.

```shell
# Common Commands

# Add your SOS number
sos,a,0000000000000#
# Get last known position
where#
# Get battery level, GSM Signal info, GPS Status
status#
# Get current configuration
param#
# Set motion alarm on / off (Does not work for me)
selalm,on#
selalm,off#
```


Somewhere on the bike, I added a [Vibration Alarm][Alarm] to ward off interested parties when I'm not around.
I highly recommend getting one, it's a cheap but effective thief deterrent - Although, I must say the battery life on the [ABUS Alarmbox][Alarm] is terrible and only lasts a month. I'm working on a mod to put this inline and run off the 60V battery - Stay tuned.

On top of these two measures, keeping it in one place is a [Kryptonite 1090][Lock].

---

## Extras

Being stuck in the forrest with a disabled Surron is not on my wish list - For this reason, I always take a [Multi-tool][Multitool] with me.

I chose a Tom-18 and it lives under the battery cover so it's always with the bike.

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/tool/pouch.jpg" caption="Under the battery cover" class="limit-height-1">}}
{{< figure src="../images/tool/tom18.jpg" caption="Tom 18" class="limit-height-1">}}
{{< figure src="../images/tool/handlebar.jpg" caption="Adjusting the break lever angle" class="limit-height-1">}}
</div>

---

## Livery 🎨

Finally, I felt the need to make my Surron more personal, I wrapped some parts in a Olive green PVC wrap.

<div style="display: flex; height: 100%; justify-content: center; align-items: center; gap: 10px">
{{< figure src="../images/livery/bike.jpg" caption="Olive green livery" class="limit-height-1">}}
{{< figure src="../images/livery/mudguard.jpg" caption="Wrapping the mudguard" class="limit-height-1">}}
</div>

---


## Gear / Protection 🦺 (Coming soon)
- Helmet
- Visor
- Chest Protector
- Knee Guards
- Boots

---

# Thoughts / Comments

Let me know what you think! [`surron@arran.nz`](mailto:surron@arran.nz)

---
