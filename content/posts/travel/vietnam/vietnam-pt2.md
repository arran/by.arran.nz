---
title: Motorbiking Vietnam Pt.2
date: 2017-09-23
lastmod: 
author: Arran

description: Hitting the road with a crunch
categories: [Travel]
tags: [Vietnam]


draft: false
---

I had bought a bike, but there was only one small problem.. The engine.

As it turns out, I should of listened to the two very persistent Vietnamese gentlemen when I was viewing the bike. They repeatedly pointed to the engine and shook their heads, even going as far as showing my an image of a head gasket. As with many Vietnamese merchants, I thought they were attempting to scam me.. they weren't.

This is how I met Vihn - an absolute champion of a mechanic. I left my bike with him and he gave me a scooter to borrow in the interim.

Later that day, I was eating dinner at The Crazy Dutch when I overheard two British guys talking about a motorbike trip to Hanoi. So naturally I joined in and asked about their plans - Lewis and Luke told me they we're considering the trip but they didn't know how to ride.

> "What better place to learn how to ride a motorbike than Ho Chi Minh City.. Right?"

After convincing them I would teach them how to ride manual, we decided to join forces and tackle Vietnam together.

I took them to Vihn's house, Luke and Lewis each bought their own shitty bike. I had also negotiated a cheaper repair for my bike since I brought Vihn business, so that worked well for me too!

## Hitting the road. (Literally)

We woke at 4.30AM to avoid peak traffic - We packed our gear, stockpiled water and filled our bikes with premium Vietnamese fuel. With only 18 kilometers of a corrugated iron jungle between us and the open road, we set off on the adventure!

But first, a **crash**.

Being the most experienced rider, I rode behind the pack and arguably got the best view for the event about to unfold. Just picture this with me - This intersection had 4 streets, 8 Lanes each. Each red light was littered with hundreds of motorbikes anxiously waiting for the green; we sat at the front.

Lights Change. Everything is normal, we're in the middle of the intersection until I hear Luke's engine roaring! He's got the clutch engaged, the bike is revving like mad.

> "Don't let the clutch go, Don't let the clutch go, Don't let the clutch go!"

He drops the clutch like a hot oven tray, the bike fucking soars into the air doing a half a back flip and slams Luke into the tarmac!

I stop my bike in front of his, shielding him from the stampede of traffic whizzing past us, I make sure he's dandy and we pull our bikes to the side of the road.

Tis but a scratch. I kick his bent foot pedals back into an acceptable shape, we take a breath and continue.

That was the *first* crash we experienced in Vietnam.