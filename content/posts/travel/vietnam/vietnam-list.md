---
title: Vietnam Packing List
date: 2017-09-22
lastmod: 2020-10-24
author: Arran

description: How much can I possibly take with me
categories: [Travel]
tags: [Vietnam]


draft: false
---

For an overseas trip, preparation is key. When packing don't forget the essentials such as a first aid kit, copies of important documents and a Forrest Gump hat.

---

* Electronics
  * BaoFeng UV-5R Radio
  * Headphones
  * Torch x 2
  * Samsung S6 Smartphone 32GB 
  * Micro USB Charger 2.4A
  * UE BOOM 2 Wireless Speaker
  * Camera
    * Nikon D5200 Body
    * 50mm Prime Lens
    * 55 - 300mm Tele Lens
    * 10 - 20mm Wide Lens
    * Charger Car / Plug
    * USB SD Card Reader
    * OTG Adapter (Micro to USB)
    * Extra Lens Cap
    * 3 x SD Cards ( 8GB x 2 , 32GB x 1 )
    * Lens Cleaning Solution
    * Manfrotto Tripod
    * Neutral Density Filters
* Clothes
  * Underwear x 12
  * Socks x 8
  * T-Shirts x 4
  * Singlets x 3
  * Shorts x 2
  * Pants x 3
  * Wool Sweater (Army Green)
  * Cotton / Poly Hoodie
  * Gorka Jacket
  * Hats

    * Forest Gump Hat
    * Camo Hat
    * Thrasher Beanie
  * Shoes
    * Hiking Boots
    * Running Shoes 
* Misc
  * Kathmandu Portable Seat
  * First Aid Kit
  * Travel Medication
  * Home-Made Phone Charger for Motorbike
  * Sleeping Bag
  * Zip Ties
  * Pens
  * Paper
  * Hacky Sack
  * 3 x Copies of Entry Visa
  * Flight Information
  * Copy of Passport and License
  * International Drivers License
  * Trip Outline and Information (To be keep inside bag)

---

I wanted to take my full camera set up with me. The problem was I knew traveling with a tramping bag, a backpack and a camera bag would be far too cumbersome for a motorbike, so I had to think of a solution.

I bought a thick piece of foam, stenciled an outline of camera and cut it out to make a snug fit for each lens and the body.

Then I whacked that in my backpack and I had a dual purpose backpack / camera bag.

> PS: 2020-10-24 -- This was way too much, I have learnt.