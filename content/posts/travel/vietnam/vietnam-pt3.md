---
title: Motorbiking Vietnam Pt.3
date: 2017-09-23
lastmod: 
author: Arran

description: Escaping Ho Chi Minh
categories: [Travel]
tags: [Vietnam]


draft: false
---

{{< figure src="../images/vietnam-bike-row.jpg" class="limit-height-1">}}

We had successfully escaped Ho Chi Minh City, we had begun our adventure to Phan Thiet!

Riding out of the city and into the country was a fascinating experience, being the first time I had seen anything like this I was in awe - Farmers working the rice fields, people herding their cattle and endless runs of markets kept me amused for the long trip.

We had made good distance but we we're still an hour away from Phan Thiet when the weather changed it's tune. We pulled over, put on our rain jackets and after much discussion we decided to battle the all-mighty rain and push onward!

But the weather fought back, flooding the roads, soaking our boots and rendering us blind with the constant downpour on our visors. After 20 minutes of a brave but stupid battle we sought shelter from the tremendous storm.

We parked underneath what looked like an abandoned gas station with a couple of Vietnamese ladies hanging out.

> "Nhà Nghỉ, Nhà Nghỉ?" (Guest House)

One of the ladies shot up, jumped on her motorbike and gestured us to follow to which we happily complied. After following her down a few muddy (previously dirt) roads we arrived at a deserted beach resort. Thank you unknown Vietnamese lady!

---

The next morning we stumbled out of our room to see the owners had prepared breakfast for the entire resort (just us). We tucked in to what looked like shrimp, crab and other seafood dishes.

We resumed our journey to Phan Thiet by passing a little village called La Gi, this was to avoid a little bit of the notorious [1A Highway](https://en.wikipedia.org/wiki/National_Route_1A_(Vietnam)). The drive turned out to be a beautiful sea-side cruise where all the local kids were stoked with our presence, excitedly waving as we went past.

## The Second Crash

As we drove on the road became more rugged, increasing the already difficult drive. I was at the front of the pack when I saw a decent mound of dirt approaching, I was slowing down to ride over the bump when suddenly Luke bumped into my right side. I turn around to see his bike wobble for a couple of meters and finally topple over, sending his face into the dirt! He went out cold.

Lewis and I rushed over, pulled the bike from his tangled legs and let him slowly come to. We were in the middle of no-where, yet strangely enough a Vietnamese man wandered out of the bush and make sure we were okay.

After all having a laugh and a drink we assessed the damage to Luke's bike, It was in pretty rough shape. We got out the spray paint and appropriately painted "RIP" on the tank.

With the steering off-center, foot pedals bent and the battery barely hanging on, we set off for a mechanic. We stopped at the first "xe máy" (Motorbike) sign and got everything fixed for a couple of dollars.

## Meeting Bong


{{< figure src="../images/vietnam-with-bong.jpg" title="Having a cold Saigon Export"class="limit-height-1">}}

Eventually we made it to Phan Thiet, Luke had decided that Vietnam and it's crazy roads was too much and wanted to go home; This is how we met Bong!
Luke sold his motorbike for 2.5M VND ($110 USD) to the keen mechanic and Lewis and I both got our brake pads replaced and chains oiled. Bong then suggested we all go for a drink by way of a drinking gesture, we hopped on our bikes and apprehensively followed.

We arrived and he invited us inside his home, I was a little worried about entering a strange mans house in a small Vietnamese town from the back of a dimly lit alley..

But it was great! Bong soon unloaded a box of beer on the dining table and we soon settled in. His wife brought out the best fried chicken I have ever had, Bong scrunched up his face when I offered him some; He preferred his chewy dried fish.

Bong's parents even came by for the impromptu party! We had more beer, learned some Vietnamese and laughed. Bong called Luke a taxi to Ho Chi Minh for his flight out the next day, Lewis and I headed to a guest house and called it a night.

It was an incredible experience I will never forget, Cheers Bong.