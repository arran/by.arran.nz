---
title: Motorbiking Vietnam Pt.1
date: 2017-09-23
lastmod: 
author: Arran

description: Arriving in Vietnam and finding a bike
categories: [Travel]
tags: [Vietnam]


draft: false
---

Arrival `2016-10-14`
Departure `2016-11-12`


I hadn't really traveled overseas, I wanted to do something outrageous for my first trip, something really strange. I decided to ride a deathtrap from Ho Chi Minh City to Hanoi.

What started as a joke between me and my parents, soon developed into an obsession. I was reading articles about similar trips, watching YouTube videos, browsing forums and remaining purposefully ignorant to the countless posts condemning such trips as "death wishes". After a couple weeks It was no longer a joke, I was convinced.

> "Oh Dad, by the way I'm going to motorbike across Vietnam in a couple of months"

Fast forward a couple of months, and I was jet lagged wandering around Kuala Lumpur, waiting for my connecting flight to Vietnam.

I had spent the day meandering though humming markets and filthy streets, stopping to chat with the welcoming locals. *"Are you living for eating, or eating for living?"* a local man inquired, to which I did not have an answer to.. I still don't.

---

## The Ho Chi Minh Hunt

Arriving in Ho Chi Minh City was an overload of the senses; the pungent smell, the sweltering heat, streets bursting full of people and the general chaos that surrounded me. With a population of 8.426 million, a whopping near double of New Zealand's it's no wonder I was overwhelmed. I was quick to adjust as I had a mission, I had to find a motorbike!

### Finding A Bike

There a few ways you could find a motorbike in Ho Chi Minh, local dealers, hostel noticeboards, Facebook groups and Craigslist.

The prices of bikes range from $150USD to $350USD depending on the condition and accessories included, Such as Bag Racks, Canvas Covers, Phone Mounts, Chargers and Helmets.

I was looking for a bargain; I was scouring Craigslist when I found a 110cc Honda Win priced at $220USD. Chris, the guy selling the bike met me at The Crazy Dutch Hostel, The bike was rough as guts, spitting smoke and a had shoddy paint job. But since it was Army Green, I loved it.

I bought that wonderful piece of shit for 4 Million VND ($175USD)

{{< figure src="../images/vietnam-my-bike2.jpg" title="110cc Honda Win"class="limit-height-1">}}