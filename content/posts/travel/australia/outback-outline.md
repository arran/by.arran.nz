---
title: The Outback Outline
date: 2018-02-10
lastmod: 
author: Arran

description: Planning for the trip of Oz
categories: [Travel]
tags: [Australia]


draft: false
---


We're traveling Australia in a pimped-out 98 Honda CR-V, roof top tent equipped and a custom kitchen in the boot. This excuse for a "4WD" will take a beating hauling ass across the land down under, with the stressed rear suspension screaming for help all 20,000 kilometers.

We left Melbourne a week ago and we've been hooning north since, we're headed up past Sydney to a Bed and Breakfast to WWOF for a bit then we're doing a full anti-clockwise rotation around Australia with only a handful of mechanical breakdowns planned.


{{< figure src="../images/opals.jpg" caption="Looking for Opal" class="limit-height-1">}}