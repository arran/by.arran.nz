---
title: Going Wayland
date: 2020-11-14
lastmod: 2020-11-15 
author: Arran

description: I've decided to go 100% on Wayland - Wish me luck
categories: [Code]
tags: [GNU/Linux, Wayland]


draft: true
---

I've just installed the bare-bones Manjaro Architect on my machine and have decided to commit to Wayland.

[Sway](https://swaywm.org") has already been my enviroment for 12 months now, however this time is different, this time I'm attempting to live without `XWayland`!


## Setting up the enviroment

There is a lot of support for Wayland nowadays, you just need to enable it.

I put my enviroment variables inside `/etc/enviroment` like so:

```bash
# Firefox
MOZ_ENABLE_WAYLAND=1
# GTK
GDK_BACKEND=wayland
# QT
QT_QPA_PLATFORM=wayland
```

> This document is WIP and will continue to be updated
