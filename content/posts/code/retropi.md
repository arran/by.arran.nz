---
title: RetroPie Setup
description: Portable, Dockable RaspberryPi sweet retro gaming
date: 2022-01-30
lastmod: 2022-01-30
author: Arran

categories: [Code]
tags: [GNU/Linux, Gaming]


draft: false
---

Here's how I made and configured my [RetroPie](https://retropie.org.uk/) gaming console!


## Installing

Firstly, [download the image](https://retropie.org.uk/download/) for the hardware and install it onto a MicroSD Card.

## Configuring

Now, slam that MicroSD card into the Pi and boot it up and follow the onscreen instructions!

### Built-in Audio

I've installed a I2S Mono amp to power a small speaker that fits inside the housing.

To configure the PI to send audio via the GPIO pins, visit [this AdaFruit page](https://learn.adafruit.com/adafruit-max98357-i2s-class-d-mono-amp/raspberry-pi-usage).

### Xbox One Wireless Adapter

To use an original Xbox One controller with the official adapter, I cloned and built [xow](https://medusalix.github.io/xow/) from source.

### 7" Screen or HDMI?

I wanted a way to use the [Official 7" Display](https://www.raspberrypi.com/products/raspberry-pi-touch-display/) and have the optional to also "dock" it, with HDMI output.

When the Pi boots it reads a config located in `/boot/config.txt` to configure the display and audio output - I needed to take advantage of this to dynamically swap the screen output on boot.

Here's the idea:

1. I created two `config.txt`s, located in `/home/pi/initDisplay/` - One for LCD mode, and one for HDMI mode. (Note the first line denoting the function)
```txt
# LCD
...
```
```
# HDMI
hdmi_force_hotplug=1
ignore_lcd=1
```

2. On boot, determine whether HDMI is plugged in, if so, overwrite the respective config into `/boot/config.txt`

Here's the basic bash script:

```bash
#!/bin/bash

HDMI_NAME=$(tvservice -n 2>&1 > /dev/null)
CURRENT_CONFIG=$(head -n1 /boot/config.txt | tr -d '# ')

function set_lcd_config() {
        echo "Setting LCD Config"
        sudo cp /home/pi/initDisplay/config_lcd.txt /boot/config.txt
}

function set_hdmi_config() {
        echo "Setting HDMI Config"
        sudo cp /home/pi/initDisplay/config_hdmi.txt /boot/config.txt
}


if [ "$CURRENT_CONFIG" == "LCD" ]; then
        echo "Current Config is LCD, Setting HDMI for Next boot"
        set_hdmi_config
        exit 0
fi


if [ "$CURRENT_CONFIG" == "HDMI" ]; then
        echo "Current Config is HDMI, Is a device present?"
        if [ "$HDMI_NAME" == "[E] No device present" ]; then
                echo "No Device present,  falling back to LCD Config"
                set_lcd_config
                sudo reboot now
        fi
fi

```

That's pretty much it for now, I'll update this page as I go along!

