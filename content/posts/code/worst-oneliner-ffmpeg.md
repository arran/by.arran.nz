---
title: The Worst One-Liner
date: 2020-11-25
author: Arran

description: Here's how I encoded an entire collection of gameplay footage.
categories: [Code]
tags: [GNU/Linux, ffmpeg]


draft: false
---

I have an external HDD with all my old gameplay footage I want to keep for my archive.

However, It's all raw from the recording software and therefore **HUGE**.

# The Solution

Use `ffmpeg` to encode the videos.

```shell
ffmpeg -i game.mp4 ~/Videos/Gameplay/game.mp4
```

But, use my AMD Graphics Card.

```shell
ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -vaapi_device /dev/dri/renderD128 -i GAME.mp4 -c:v h264_vaapi -profile:v high -level 41 -c:a copy -b 15000k GAME.mp4
```

## Automate it all

I don't want to type this command for every file, that'd take years.

### List the `mp4` files.

```shell
$ find . -type f -name "*.mp4" -print0
```

- `-type f` Find only files.
- `-name` Match this name.
- `-print0` Use a NULL character instead of a newline.

### Prepare the output file name.

Here's how I turned the input path into the filename with `cut` and the delimiter (`-d`) argument.

For example `./Battlefield1/EpicMoment001.mp4` into `EpicMoment001.mp4`

```shell
$ cut -d \ -f 3
```
> Note: The above command takes into consideration the depth of the folder structure.

### Using xargs

Using `xargs` we can take the input from the `find` command and execute a statement for every entry.

```
$ xargs -I {} -0 sh -c "..."
```

- `-I {}` Map this input parameter to this sequence of characters
- `-0` Terminate entries by a NULL character, usefull when you have filenames with spaces.
- `sh -c "..."` Execute this command for each instance.

### The final monstrosity!

```shell
# Working from the current directory: /media/usb/GAMEPLAY
$ find . -type f -name "*.mp4" -print0 | xargs -I {} -0 sh -c \
    "ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -vaapi_device /dev/dri/renderD128 \
    -i \"{}\" -c:v h264_vaapi -profile:v high -level 41 -c:a copy -b 15000k** \
    \"/home/arran/Videos/Gameplay/{} | cut -d / -f 3`\""
```

# I've Won.. But at what cost?

This was my first endevour with `ffmpeg` and I'm sure it won't be the last, time to look for a better solution to bulk operations.
