---
title: "Done simply with Webhook"
description: How this blog is automatically deployed with Webhook
date: 2020-11-17T12:47:40+01:00
lastmod: 2020-11-21
author: Arran

categories: [Code]
tags: [GNU/Linux, Gitea, Uberspace, Webhook, Hugo]

draft: false
---

Ever wanted to run an arbitrary script from anywhere with flexible authentication? Let me introduce [Webhook][wh]!

I'll show you how I setup this Hugo site to automatically render and deploy everytime I push to `master` on a [Gitea][gitea] instance.

##  Install

Go download the [Webhook][wh] binary, or build it yourself.

```shell
$ go get github.com/adnanh/webhook
```

### Configuration

Create `hooks.json` and a folder to contain everything.

```shell
$ mkdir webhook && cd webhook
$ touch hooks.json
```

Inside `hooks.json`, copy this template and update the location of the script you wish to execute.

```json
[
  {
    "id": "redeploy-blog",
    "execute-command": "/home/arran/hugo_websites/redeploy.sh",
    "command-working-directory": "/home/arran/",
  }
]
```

For the curious, here's what inside my `redeploy.sh`.

```bash
#!/bin/env bash

REPO="${HOME}/hugo_websites/by.arran.nz/"

git -C ${REPO} fetch && git -C ${REPO} rebase
git -C ${REPO} submodule update --remote --rebase  
HUGO_CACHEDIR=${HOME}/tmp hugo --cleanDestinationDir --destination /var/www/virtual/${USER}/html --source ${REPO}
```

## Supervisor

[Uberspace][uber] uses [Supervisor][super] to manage services. To create a new service, make a file located at `~/etc/services.d/webook.ini`.

```ini
[program:webhook]
directory=%(ENV_HOME)s/webhook
autostart=true
autorestart=true
startretries=3
command=/home/arran/go/bin/webhook -hooks hooks.json -logfile out.log -hotreload
```

After creating the configuration, tell supervisord to refresh its configuration and start the service:

```shell
$ superviserctl reread
$ superviserctl update
$ supervisorctl status
webhook                          RUNNING   pid 13102, uptime 0:35:37
```
Check out the [Uberspace supervisord manual](https://manual.uberspace.de/daemons-supervisord.html) for further details.

## Uberspace Backend

Now configure the [Uberspace][uber] backend to point port `80` to [Webhook][wh] running under port `9000` - This is the default.

```shell
$ uberspace web backend set arran.uber.space --http --port 9000
Set backend for arran.uber.space/ to port 9000; please make sure something is listening!
You can always check the status of your backend using "uberspace web backend list".
```

For more infomation, check out the [Uberspace Manual](https://manual.uberspace.de/web-backends.html)

## Sanity Check 

Now that a service is running and is exposed to the internet, test it.

Considering the webhook is named `redeploy-blog`, send a `POST` to:

```shell
$ curl -X POST https://arran.uber.space/hooks/redeploy-blog
```

Confirm the webhook was successful by checking the logs located at `~/webhook/out.log` as defined in `webhook.ini`.

```shell
$ cat out.log
...
[webhook] 2020/11/17 13:24:05 [7f6850] redeploy-blog got matched
[webhook] 2020/11/17 13:24:05 [7f6850] redeploy-blog hook triggered successfully
...
```

## Secure the webhook 🔐 

> Note: Make sure to setup [Webhook][wh] with `https` before implementing this step - Don't go sending your secrets over `http`!

You may of noticed anyone can call this webhook - Let's fix that.

This blog's repo is currently hosted at [Codeberg](https://codeberg.org/), which is a [Gitea][gitea] instance.
Configure [Webhook][wh] to read the `secret` from a [Gitea][gitea] instance.

Add a `trigger-rule` to the `hooks.json` we created earlier - Replace the secret with your own.
```json
[
  {
    "id": "redeploy-blog",
    "execute-command": "/home/arran/hugo_websites/redeploy.sh",
    "command-working-directory": "/home/arran/",
    "trigger-rule":
    {
      "and":
      [
        {
          "match":
          {
            "type": "value",
            "value": "____GITEA_SECRET____",
            "parameter":
            {
              "source": "payload",
              "name": "secret"
            }
          }
        },
        {
          "match":
          {
            "type": "value",
            "value": "refs/heads/master",
            "parameter":
            {
              "source": "payload",
              "name": "ref"
            }
          }
        }
      ]
    }
  }
]
```

Now [Webhook][wh] will only allow execution when the request matches the `trigger-rule`.

For further examples, reference the [Hook Examples](https://github.com/adnanh/webhook/blob/master/docs/Hook-Examples.md).

### Setting up Gitea

Head on over to your Gitea instance's repo `Repo > Settings > Webhooks`, punch in the url and secret and you're good to go!

> This was the first post successfully deployed with this new deployment system! 🥳 

[wh]: https://github.com/adnanh/webhook
[uber]: https://uberspace.de
[super]: https://supervisord.org/
[gitea]: https://gitea.io
