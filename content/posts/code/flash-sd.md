---
title: No BS - Flashing an SD Card
date: 2020-11-09
lastmod: 
author: Arran

description: Dead simple way to flash an SD Card on GNU/Linux
categories: [Code]
tags: [GNU/Linux]


draft: false
---

After seeing the ocean of search results for a trivial task, I've decided to document how I
flash my SD Cards.


## Locate the SD Card

```shell
$ lsblk
NAME                                            MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
├─sdd1                                            8:49   1   256M  0 part  /run/media/card/boot
└─sdd2                                            8:50   1  14.6G  0 part  /run/media/card/rootfs
```

If you're unsure, use `dmesg` after you've mounted the device to locate the device.

```shell
$ dmesg | tail
[ 3233.883672] usb-storage 4-6.2:1.0: USB Mass Storage device detected
[ 3233.884067] scsi host6: usb-storage 4-6.2:1.0
[ 3234.915182] scsi 6:0:0:0: Direct-Access     Generic- USB3.0 CRW   -SD 1.00 PQ: 0 ANSI: 4
[ 3234.928448] scsi 6:0:0:1: Direct-Access     Generic- USB3.0 CRW   -SD 1.00 PQ: 0 ANSI: 4
[ 3234.928689] sd 6:0:0:0: Attached scsi generic sg2 type 0
[ 3234.928987] scsi 6:0:0:1: Attached scsi generic sg3 type 0
[ 3236.294463] sd 6:0:0:1: [sdd] 31116288 512-byte logical blocks: (15.9 GB/14.8 GiB)
[ 3236.297764] sd 6:0:0:1: [sdd] Write Protect is off
[ 3236.297765] sd 6:0:0:1: [sdd] Mode Sense: 2f 00 00 00
[ 3236.300142] sd 6:0:0:1: [sdd] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
[ 3236.305350] sd 6:0:0:0: [sdc] Attached SCSI removable disk
[ 3236.335973] sd 6:0:0:1: [sdd] Attached SCSI removable disk
```

# Write the image

Using `dd`, write the image to the device.

`bs` = 4MB Block Size

`if` = Input file

`of` = Output file (device)


```shell
$ dd bs=4M of=/dev/sdd if=/home/arran/Img/2020-08-20-raspios-buster-armhf-lite.img
440+0 records in
440+0 records out
1845493760 bytes (1.8 GB, 1.7 GiB) copied, 190.45 s, 9.7 MB/s

$ sync
```

