---
title: Crowd Pleaser
date: 2017-07-04
lastmod: 
author: Arran

description: My first Android game.
categories: [Game Development]
tags: [Android, Unity, PixelArt]


draft: false
---


After participating in Ludum Dare 38 and creating [Roo's World](https://azza292.itch.io/roos-small-world) in 48 hours, I was convinced I could design, draw and polish a simple Android game in 5 days, so that's what I did.

The idea was as simple as you could ask, a tap to jump reaction based game with increasing difficulty over time.

I got to work immediately after breakfast, sitting down and creating a 2D array of objects, adding a simple sprite and looping through them to create the wave effect.

I used a free pixel animator called [Piskel](http://www.piskelapp.com/) to create my animated sprites. I then used a palette swapping technique to generate a more visually appealing crowd while saving a ton of time.

Next I added a wave selector that randomly triggered waves from each direction, A player character and made the rest of the art assets.

Then after getting the game play right, adding high scores and achievements I was done!

Overall I really enjoyed the process, I learnt a few useful techniques and I would recommended to every beginner game dev to put down that enormous project for one week and finish something small.

The UI was supplied from every Game Developers best-friend, Kenney. [UI Pack](https://kenney.nl/assets/ui-pack)

The fantastic music was composed by [Wyver9](https://wyver9.bandcamp.com/)

---

Tap the screen and join a Mexican wave hurdling across your screen, then do that again, and a few more times.

[Install from Google Play](https://play.google.com/store/apps/details?id=com.ArranSmith.CrowdPleaser&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)