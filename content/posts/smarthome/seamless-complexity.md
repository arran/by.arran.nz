---
title: The orchestration of seamless complexity 
date: 2023-09-15
author: Arran

description: The seamless integration of Home Assistant, Zigbee, and HDMI-CEC orchestrates a complex choreography of automation in my smart home.
categories: [Code, Smarthome]
tags: [GNU/Linux]


draft: false
---

It's all starting to fall into place; the arduous research, sifting through documentation, and good old trial and error are paying off in spades. The house is coming alive, becoming a part of our daily lives. We're in the future, and it's free and liberating.

I've been building my smarthome on open standards and technology, it's self-hosted and doesn't cost anything but a few watts.

This post describes a **vertical slice** of one single automation within my home, and an appreciation of the many layers of free and open source software.

The goal is simple enough, I would like the lights to turn on when a movie is paused, and off again when the movie is resumed.

It's fair to say that Home Assistant is the conductor, but it's not without it's orchestra - Within the space of ~100ms this elegant symphony begins and concludes.

Our instruments are: Infrared, HDMI-CEC, USB, Kodi, HTTP, Home Assistant, Zigbee.

Here's how it all fits together:

### The Flow
{{<mermaid>}}
flowchart TD
    TVR(TV Remote - Pause button pushed) --> |Infrared Signal| TV
    TV(LG TV) -->|HDMI-CEC| AV(ONKYO AV RECEIVER)
    AV -->|HDMI-CEC| P8(Pulse Eight HDMI-CEC Adapter)
    P8 --> |USB| KODI(Kodi running on Fedora/Linux)
    KODI --> |Pauses Media| KODI
    HA(Home Assistant running on HAOS) --> |"HTTP (Listening for events)"| KODI
    HA --> |"ON 0x0004 Zigbee"| ZBC((Zigbee Mesh))
    ZBC --> |"ON 0x0004 Zigbee"| ZG("Zigbee Li ght group (0x0004)")
    ZG --> |"ON"| Z1("Lamp 1")
    ZG --> |"ON"| Z2("Lamp 2")
    ZG --> |"ON"| Z3("Lamp 3")
{{</mermaid>}}


#### Breakdown

1. The TV remote sends an infrared signal to the TV when the pause button is pushed.
2. The TV, using HDMI-CEC, communicates with the ONKYO AV RECEIVER.
3. The AV RECEIVER, also using HDMI-CEC, communicates with the Pulse Eight HDMI-CEC Adapter.
4. The Pulse Eight HDMI-CEC Adapter connects to Kodi running on Fedora/Linux via USB.
5. Kodi, using libCEC to listen to HDMI-CEC events pauses the media playback.
6. Home Assistant running on HAOS listens for events via HTTP.
7. Home Assistant sends an "ON 0x0004 Zigbee" command to the Zigbee Mesh (ZBC).
8. Zigbee Mesh forwards the "ON 0x0004 Zigbee" command to the Zigbee Light group (ZG).
9. Zigbee Light group (ZG) controls Lamp 1, Lamp 2, and Lamp 3 by sending "ON" commands.


## Conclusion

The appreciation I have for modern technology and the free and open source community is immense. This automation, while but a trivial convenience, showcases years of volunteer work, interoperation between a variety of technologies and what's most important in my opinion, is that as hackers, tinkerers and engineers we can stand on the shoulders of giants and achieve amazing things together - Even if that thing, is something superfluous like automating your lights..

I hope you feel as inspired as I do, and can use this as a inspiration for how you appreciate and approach your home automations.

## Technology Used:

- [Pulse Eight](https://www.pulse-eight.com/)
    - [HDMI-CEC Adapter](https://www.pulse-eight.com/p/104/usb-hdmi-cec-adapter])
    - [libCEC](https://github.com/Pulse-Eight/libcec/releases)
- [Home Assistant](https://www.home-assistant.io/)
    - [ZHA Integration](https://www.home-assistant.io/integrations/zha/)
    - [Kodi Integration](https://www.home-assistant.io/integrations/kodi/)
- [Kodi](https://kodi.tv/)
    - [CEC](https://kodi.wiki/view/CEC)

