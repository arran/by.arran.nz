---
title: YALK35 - Yet Another LK35
date: 2020-10-20T23:08:08+02:00
lastmod: 
author: Arran

description: How I am making the perfect outdoor backpack (for me)
categories: [Gear]
tags: [LK35]


draft: true
---

{{< figure src="images/img.png" title="Classic LK35"class="limit-height-1" attr="Attr: Arran" attrlink="https://arran.nz">}}


It's a beast, it will endure anything I throw at it!
